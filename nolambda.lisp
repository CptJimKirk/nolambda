;;; define a function and lambda form that allows for implicit arguments
;;; {+ x 2}
;;; allowable anywhere a lambda would be permitted
;;; ({+ x 2} 3)
;;; (mapcar {+ x 2} '(1 2 3))
;;; etc.
;;;
;;;
;;; Implicit args are x y and z. If you want to pass 1 arg, mention x somewhere in your function body
;;; if you want to pass 2 args, mention x y
;;; if you want to pass 3 args, mention x y z
(defpackage #:nolambda
  (:use #:cl #:str)
  (:export :def-k))

(in-package :nolambda)
(defconstant +open-bracket+  #\[)
(defconstant +close-bracket+ #\])
(defconstant +open-brace+    #\{)
(defconstant +close-brace+   #\})
(defconstant +open-paren+    #\()
(defconstant +close-paren+    #\))

(defmacro def-k (name &rest body)
  "(def-k my-fun {+ x (- z y)}) define a function using the implicit args form"
  `(defun ,name (&rest args)
     (apply ,@body args)))

(defun def-k-reader (stream char)
  "reader macro to read in the {...} function form"
  (declare (ignore char))
  (let ((*readtable* (copy-readtable)))
    (set-macro-character +close-brace+ (get-macro-character +open-paren+ nil))
    (let* ((result (read-delimited-list +close-brace+ stream t))
           (arg-list (args result)))
      `(lambda ,arg-list ,result))))


(defun flatten (xs)
  "flatten a list"
  (if (not (consp xs))
      xs
      (let ((fsx (first xs)))
        (cond ((or (atom fsx) (null fsx))
               (cons fsx (flatten (rest xs))))
              ((listp (first xs))
               (append (flatten fsx)
                       (flatten (rest xs))))))))


(defun symbol-sort (symbols)
  "sort symbols alphabetically"
  (sort symbols (lambda (x y)
                  (char-lessp (char (symbol-name x) 0)
                              (char (symbol-name y) 0)))))

(defun args (s)
  "return an argument list (x y z) to be placed as the arglist for a lambda form `(lambda ,arg-list ...)"
  (mapcar (lambda (x) (intern (symbol-name  x) *package*))
          (symbol-sort
           (remove 'nil
                   (remove-duplicates
                    (flatten (grab-args s)))))))


(defun grab-args (s)
  "grab args looks recurisvely into list forms for any mention of arguments x y and z, excluding lambda forms"
  (when (not (eq 'lambda (first s)))
    (mapcar
     (lambda (x)
       (cond ((stringp x) 'NIL)
             ((symbolp x) (reverse (member x '(z y x) :test #'string=)))
             ((listp x)   (grab-args x))))
     s)))

(set-macro-character +open-brace+ 'def-k-reader)


