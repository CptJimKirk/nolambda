;;;; nolambda.asd

(asdf:defsystem #:nolambda
                :description "no use for a lambda"
                :serial t
                :depends-on ("str")
                :components ((:file "nolambda")))
